#!/bin/bash

isArgument=false;

for arg in "$@"
    do
        if [ $arg == 'multipull' ]
        then
            isArgument=true;
            echo -e "\e[1;33;40mtime-manager\e[0m\e[1;32m\e[0m";
            git pull origin master;

            echo -e "\e[1;33;40mtester\e[0m\e[1;32m\e[0m";
            cd tester && git pull origin master && cd ./../;

            echo -e "\e[1;33;40mfrontend\e[0m\e[1;32m\e[0m";
            cd frontend && git pull origin master && cd ./../;

            echo -e "\e[1;33;40mbackend -> migrator\e[0m\e[1;32m\e[0m";
            cd backend/migrator && git pull origin master && cd ./../../;

            echo -e "\e[1;33;40mbackend -> search\e[0m\e[1;32m\e[0m";
            cd backend/search && git pull origin master && cd ./../../;

            echo -e "\e[1;33;40mbackend -> report\e[0m\e[1;32m\e[0m";
            cd backend/report && git pull origin master && cd ./../../;

            echo -e "\e[1;33;40mbackend -> statistics\e[0m\e[1;32m\e[0m";
            cd backend/statistics && git pull origin master && cd ./../../;
        fi
    done

if [ $isArgument == false ]
    then
        echo -e "\e[1;33;40mtime-manager\e[0m\e[1;32m\e[0m";
        git status;

        echo -e "\e[1;33;40mtester\e[0m\e[1;32m\e[0m";
        cd tester && git status && cd ./../;

        echo -e "\e[1;33;40mfrontend\e[0m\e[1;32m\e[0m";
        cd frontend && git status && cd ./../;

        echo -e "\e[1;33;40mbackend -> migrator\e[0m\e[1;32m\e[0m";
        cd backend/migrator && git status && cd ./../../;

        echo -e "\e[1;33;40mbackend -> search\e[0m\e[1;32m\e[0m";
        cd backend/search && git status && cd ./../../;

        echo -e "\e[1;33;40mbackend -> report\e[0m\e[1;32m\e[0m";
        cd backend/report && git status && cd ./../../;

        echo -e "\e[1;33;40mbackend -> statistics\e[0m\e[1;32m\e[0m";
        cd backend/statistics && git status && cd ./../../;
    fi
