cd /var/www/html/;
npm install;
npm run build;

if [[ ${NODE_ENV} == 'development' ]];then
  echo -e "\e[35mdevelopment mode..\e[0m";
  npm run start:dev;
else
    echo -e "\e[35mproduction mode..\e[0m";
	npm run start:prod;
fi


