#!/bin/bash

install=false;
update=false;

for arg in "$@"
    do
        if [ $arg == 'install' ]
        then
            install=true;
        elif [ $arg == 'update' ]
        then
            update=true;   
        fi
    done

if [ $install == true ]
    then
        cp -u .env.example .env;

        mkdir -p backend/migrator;
        git clone git@gitlab.com:time-manager-group/application/migrator.git backend/migrator;
        cp -u  backend/migrator/.env.example  backend/migrator/.env;

        mkdir -p backend/report;
        git clone git@gitlab.com:time-manager-group/application/report.git backend/report;
        cp -u  backend/report/.env.example  backend/report/.env;

        mkdir -p backend/search;
        git clone git@gitlab.com:time-manager-group/application/search.git backend/search;
        cp -u  backend/search/.env.example  backend/search/.env;

        mkdir -p backend/statistics;
        git clone git@gitlab.com:time-manager-group/application/statistics.git backend/statistics;

        mkdir -p frontend;
        git clone git@gitlab.com:time-manager-group/application/frontend.git frontend;

        mkdir tester;
        git clone git@gitlab.com:time-manager-group/tester.git tester;
        cp -u tester/.env.example tester/.env;

        mkdir -p storage;

        docker kill $(docker ps -q);
        docker network prune -f;
        docker system prune -f;

        docker-compose build && docker-compose up -d --force-recreate;


        docker-compose exec -u 1000 app_back_migrator \
                        composer install;

        docker-compose exec -u 1000 app_back_migrator \
                        php /var/www/html/artisan key:generate --ansi;

        docker-compose exec -u 1000 app_back_migrator \
                        php /var/www/html/artisan migrate;


        docker-compose exec -u 1000 app_back_report \
                        composer install;

        docker-compose exec -u 1000 app_back_report \
                        php /var/www/html/artisan key:generate --ansi;


        docker-compose exec -u 1000 app_back_search \
                        composer install;

        docker-compose exec -u 1000 app_back_search \
                        php /var/www/html/artisan key:generate --ansi;


        docker-compose exec -u 1000 app_nginx_front \
                        npm install --legacy-peer-deps;

        docker-compose exec -u 1000 app_nginx_front \
                        npm run build;


        cd tester && bash test.sh save && cd ./../;


        docker-compose exec -u 1000 app_back_migrator \
                        php /var/www/html/artisan config:clear;

        docker-compose exec -u 1000 app_back_migrator \
                        php /var/www/html/artisan cache:clear;

        docker-compose exec -u 1000 app_back_migrator \
                        php /var/www/html/artisan config:clear;


        docker-compose exec -u 1000 app_back_report \
                        php /var/www/html/artisan key:generate --ansi;

        docker-compose exec -u 1000 app_back_report \
                        php /var/www/html/artisan cache:clear;

        docker-compose exec -u 1000 app_back_report \
                        php /var/www/html/artisan config:clear;


        docker-compose exec -u 1000 app_back_search \
                        php /var/www/html/artisan key:generate --ansi;

        docker-compose exec -u 1000 app_back_search \
                        php /var/www/html/artisan cache:clear;

        docker-compose exec -u 1000 app_back_search \
                        php /var/www/html/artisan config:clear;


       docker-compose ps;

       echo -e "\e[1;33;40mApplication URL: http://time-manager.ru.172.20.128.2.nip.io/\e[0m";
    fi

if [ $update == true ]
    then
        docker-compose down;

            echo -e "\e[1;33;40mtime-manager\e[0m\e[1;32m\e[0m";
            git pull origin master;

            echo -e "\e[1;33;40mtester\e[0m\e[1;32m\e[0m";
            cd tester && git pull origin master && cd ./../;

            echo -e "\e[1;33;40mfrontend\e[0m\e[1;32m\e[0m";
            cd frontend && git pull origin master && cd ./../;

            echo -e "\e[1;33;40mbackend -> migrator\e[0m\e[1;32m\e[0m";
            cd backend/migrator && git pull origin master && cd ./../../;

            echo -e "\e[1;33;40mbackend -> search\e[0m\e[1;32m\e[0m";
            cd backend/search && git pull origin master && cd ./../../;

            echo -e "\e[1;33;40mbackend -> report\e[0m\e[1;32m\e[0m";
            cd backend/report && git pull origin master && cd ./../../;

            echo -e "\e[1;33;40mbackend -> statistics\e[0m\e[1;32m\e[0m";
            cd backend/statistics && git pull origin master && cd ./../../;



        docker-compose run -u 1000 app_back_migrator \
                        composer install;
        docker-compose run  -u 1000 app_back_migrator \
                        php /var/www/html/artisan migrate;


        docker-compose run -u 1000 app_back_report \
                        composer install;

        docker-compose run -u 1000 app_back_search \
                        composer install;

        docker-compose run -u 1000 app_nginx_front \
                        npm install --legacy-peer-deps;
        docker-compose run -u 1000 app_nginx_front \
                        npm run build;


        docker-compose run -u 1000 app_back_migrator \
                        php /var/www/html/artisan config:clear;

        docker-compose run -u 1000 app_back_migrator \
                        php /var/www/html/artisan cache:clear;

        docker-compose run -u 1000 app_back_migrator \
                        php /var/www/html/artisan config:clear;


        docker-compose run -u 1000 app_back_report \
                        php /var/www/html/artisan cache:clear;

        docker-compose run -u 1000 app_back_report \
                        php /var/www/html/artisan config:clear;


        docker-compose run -u 1000 app_back_search \
                        php /var/www/html/artisan cache:clear;

        docker-compose run -u 1000 app_back_search \
                        php /var/www/html/artisan config:clear;
 	
 	docker-compose down;
        docker-compose up -d;

        cd tester && bash test.sh save && cd ./../;
	
        echo -e "\e[1;33;40mUpdate \`Time manager\` final.\e[0m";

        echo -e "\e[1;33;40mApplication URL: http://time-manager.ru.172.20.128.2.nip.io/\e[0m";
    fi
